from flask import Flask, render_template,request
app = Flask(__name__)


@app.route("/")
def home():	
	return render_template('index.html')





#main categories-------------------------------

inspirationalurls = ['https://www.youtube.com/embed/-sUKoKQlEC4','https://www.youtube.com/embed/GVG4wgCqeEQ',
	 'https://www.youtube.com/embed/LG8wGsOL15I' ,'https://www.youtube.com/embed/jwjGw3BCryI' ,
	 ]
@app.route("/inspirational/")
def inspirational():	
	return render_template('inspirational.html',inspirationalurls=inspirationalurls)
@app . errorhandler (404)
def page_not_found ( error ):
	return render_template('error.html'),404
	

timeurls = ['https://www.youtube.com/embed/NCJb9-wbH6s', 
	'https://www.youtube.com/embed/AA79o6QcrTs', 'https://www.youtube.com/embed/gRkDVR80smU' ,
 	'https://www.youtube.com/embed/aOWSmNcEBCc' , 'https://www.youtube.com/embed/rm5bkMLwMcc',
  	'https://www.youtube.com/embed/x5lsGBwSGEI', 'https://www.youtube.com/embed/Tpv7PYUvdyw', 
	'https://www.youtube.com/embed/NWT5IQKN-WA', 'https://www.youtube.com/embed/jBfhefEWd-c', 
 	'https://www.youtube.com/embed/8aD01ndbRRo', 'https://www.youtube.com/embed/n_yV3KBzxx4'
  , 'https://www.youtube.com/embed/6Nv0Nc33-28' ]
erictimeurls = ['https://www.youtube.com/embed/Q5KVvz5nn_8','https://www.youtube.com/embed/6Nv0Nc33-28','https://www.youtube.com/embed/NWT5IQKN-WA']
danitimeurls = ['https://www.youtube.com/embed/NCJb9-wbH6s','https://www.youtube.com/embed/sBcILLGoWP0','https://www.youtube.com/embed/JCgXsfj2W-I']
anthonytimeurls = ['https://www.youtube.com/embed/jw0pM3sElEo','https://www.youtube.com/embed/gRkDVR80smU','https://www.youtube.com/embed/_mZDDYgi0AU']
jimrhontimeurls = ['https://www.youtube.com/embed/aOWSmNcEBCc','https://www.youtube.com/embed/nqt8Sm9prTU','https://www.youtube.com/embed/GQu9C5JV0lE']
brendontimeurls = ['https://www.youtube.com/embed/jBfhefEWd-c','https://www.youtube.com/embed/8aD01ndbRRo']
bobproctortimeurls = ['https://www.youtube.com/embed/1Iwsyi33RTY','https://www.youtube.com/embed/GYom_eXy254']
patricktimeurls = ['https://www.youtube.com/embed/Tpv7PYUvdyw','https://www.youtube.com/embed/pXOp81dV2RY','https://www.youtube.com/embed/8rtwwfxYCAo']
seminars = ['https://www.youtube.com/embed/aOWSmNcEBCc','https://www.youtube.com/embed/rm5bkMLwMcc','https://www.youtube.com/embed/euSrAYrh178']
shortlength = ['https://www.youtube.com/embed/NCJb9-wbH6s','https://www.youtube.com/embed/Tpv7PYUvdyw','https://www.youtube.com/embed/NWT5IQKN-WA',
'https://www.youtube.com/embed/jBfhefEWd-c','https://www.youtube.com/embed/8aD01ndbRRo','https://www.youtube.com/embed/n_yV3KBzxx4']
@app.route("/time-management/")
@app.route("/time-management/<selector>")
def timemanagement(selector=None):
	return render_template('timemanagement.html',timeurls=timeurls,erictimeurls=erictimeurls,danitimeurls=danitimeurls,anthonytimeurls=anthonytimeurls,
		jimrhontimeurls=jimrhontimeurls,patricktimeurls=patricktimeurls,bobproctortimeurls=bobproctortimeurls,brendontimeurls=brendontimeurls,
		seminars=seminars,shortlength=shortlength,selector=selector)


goalurls = ['https://www.youtube.com/embed/gzx3qhEgyZQ', 
'https://www.youtube.com/embed/r5jy-oDXYSU', 'https://www.youtube.com/embed/T3WF0Zf1I5A' ,
 'https://www.youtube.com/embed/uOSkV2PAb1Q' , 'https://www.youtube.com/embed/XzOk6MgsKBQ',
  'https://www.youtube.com/embed/4Y717JgsMU8', 'https://www.youtube.com/embed/4KXRHLcX8GM', 
  'https://www.youtube.com/embed/2Jo7ZdYtjsc', 'https://www.youtube.com/embed/54aFTZ9POw4', 
  'https://www.youtube.com/embed/kYQn69Te8OA', 'https://www.youtube.com/embed/YuObJcgfSQA'
  , 'https://www.youtube.com/embed/kmM_XkxuCxY' ]
@app.route("/goal-setting/")
def goalsetting():	
	return render_template('goal-setting.html',goalurls=goalurls)


financeurls = ['https://www.youtube.com/embed/kSoO2KjVVG4', 
'https://www.youtube.com/embed/IHlXgCaJvR4', 'https://www.youtube.com/embed/qcTKMkWpXAI' ,
 'https://www.youtube.com/embed/k_IpOxyPZp4' , 'https://www.youtube.com/embed/_NeKV7_Ez1I',
  'https://www.youtube.com/embed/hjVsLPaUW5k', 'https://www.youtube.com/embed/cNgBWAFV9gU', 
  'https://www.youtube.com/embed/mXYyEjRfO18', 'https://www.youtube.com/embed/-t4UZ5B35g4', 
  'https://www.youtube.com/embed/1zcO-BGkudk', 'https://www.youtube.com/embed/mU7h3PCmSOc'
  , 'https://www.youtube.com/embed/oz26bQBOPWI' ]
@app.route("/finances/")
def finances():	
	return render_template('finances.html',financeurls=financeurls)

businessurls = ['https://www.youtube.com/embed/nmsqxiJETHs',
 'https://www.youtube.com/embed/9Ip9ecPSJRI', 'https://www.youtube.com/embed/8sK06zkD41U' ,
 'https://www.youtube.com/embed/k_IpOxyPZp4' , 'https://www.youtube.com/embed/_NeKV7_Ez1I',
  'https://www.youtube.com/embed/hdGKAHvgBqo', 'https://www.youtube.com/embed/QuF-VQGO0AE', 
  'https://www.youtube.com/embed/QgGTIQ_03wA', 'https://www.youtube.com/embed/oMJYKg5um3U', 
  'https://www.youtube.com/embed/RY0Bc-I1bAU', 'https://www.youtube.com/embed/rWUeV7vWTsA'
  , 'https://www.youtube.com/embed/xNFzTvrrmJQ' ]
@app.route("/business/")
def business():	
	return render_template('business.html',businessurls=businessurls)

relationshipsurls = ['https://www.youtube.com/embed/cu71b34O9dI',
 'https://www.youtube.com/embed/u99qmt0j5A4', 'https://www.youtube.com/embed/aYBWeOvnJrQ' ,
 'https://www.youtube.com/embed/2prHYfx0qTU' , 'https://www.youtube.com/embed/Eo3dJ2o9Nnk',
  'https://www.youtube.com/embed/L3lYDrERSLU', 'https://www.youtube.com/embed/rNVtxTwd9zc', 
  'https://www.youtube.com/embed/LsqTZZUM--M', 'https://www.youtube.com/embed/RYUFuU_Ii2I', 
  'https://www.youtube.com/embed/FpjND831T2w', 'https://www.youtube.com/embed/HOw4HDd0ZrM'
  , 'https://www.youtube.com/embed/aLSqzMqI8tA' ]
@app.route("/relationships/")
def relationships():	
	return render_template('relationships.html',relationshipsurls=relationshipsurls)

spiritualurls = ['https://www.youtube.com/embed/LVI8O8Up7sQ',
 'https://www.youtube.com/embed/rplnVV7I6nc', 'https://www.youtube.com/embed/LpDkon5oZLc' ,
 'https://www.youtube.com/embed/yAiTMaipAG8' , 'https://www.youtube.com/embed/NZBlGCdhNwY',
  'https://www.youtube.com/embed/hSLji-OHrqE', 'https://www.youtube.com/embed/00y6vzfEclQ', 
  'https://www.youtube.com/embed/TbZ7nZX3MLI', 'https://www.youtube.com/embed/v1wZQ2nkDeI', 
  'https://www.youtube.com/embed/b8zav9qgbF4', 'https://www.youtube.com/embed/hxhyG0Tmr6U'
  , 'https://www.youtube.com/embed/Y2vQMxg20Dw' ]
@app.route("/spiritual/")
def spiritual():	
	return render_template('spiritual.html',spiritualurls=spiritualurls)

#main categories-------------------------------


#coaches videos-------------------------------
danivideourls = ['https://www.youtube.com/embed/uKLch9JrZOc','https://www.youtube.com/embed/hds2aBQ_Ws8','https://www.youtube.com/embed/wTeHmeP_Irc']
@app.route("/danijohnson/videos/")
def danijohnson():	
	return render_template('danijohnson-videos.html',danivideourls=danivideourls)

anthonyvideourls = ['https://www.youtube.com/embed/zoN0D337GaA','https://www.youtube.com/embed/BwFOwyoH-3g','https://www.youtube.com/embed/ZB6yxZ5w1j8']
@app.route("/anthonyrobbins/videos/")
def anthonyrobbins():
	return render_template('anthonyrobbins-videos.html',anthonyvideourls=anthonyvideourls)

ericvideourls = ['https://www.youtube.com/embed/YJaPYRAs2SI','https://www.youtube.com/embed/sWlXsEj1RpM','https://www.youtube.com/embed/HkR5pSL_IPo']
@app.route("/ericthomas/videos/")
def ericthomas():	
	return render_template('ericthomas-videos.html',ericvideourls=ericvideourls)

brendonvideourls = ['https://www.youtube.com/embed/TDDhn4tnWlo','https://www.youtube.com/embed/FZHOkxzW9cY','https://www.youtube.com/embed/uj7nihat-48']
@app.route("/brendon-burchard/videos/")
def brendonburchard():	
	return render_template('brendonburchard-videos.html',brendonvideourls=brendonvideourls)

#coaches videos-------------------------------


#success stories-------------------------------

@app.route("/danijohnson/story/")
def danijohnsonstory():	
	return render_template('danijohnson-story.html')

@app.route("/anthonyrobbins/story/")
def anthonyrobbinsstory():	
	return render_template('anthonyrobbins-story.html')


@app.route("/ericthomas/story/")
def ericthomasstory():	
	return render_template('ericthomas-story.html')

@app.route("/patrickbetdavid/story/")
def patrickbetdavidstory():	
	return render_template('patrickbetdavid-story.html')

#success stories-------------------------------

if __name__ == " __main__ " :
	app . run ( host = '0.0.0.0' , debug = True )